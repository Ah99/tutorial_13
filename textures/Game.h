#ifndef GAME_H
#define GAME_H

#include "Mesh.h"
#include "Model.h"
#include "singleton.h"
#include "SpriteFont.h"

#include <vector>
#include <future>

class Game : public Singleton<Game>
{
public:
	Game() {}
	~Game() {
		Release();
	}
	void Update(float dTime);
	void Render(float dTime);
	void Initialise();
	void Release();
	LRESULT WindowsMssgHandler(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);

	const DirectX::SimpleMath::Vector3 mDefCamPos = DirectX::SimpleMath::Vector3(-5, 2, -8);
	DirectX::SimpleMath::Vector3 mCamPos = DirectX::SimpleMath::Vector3(-2, 2, -9);
	Model mBox, mQuad, mClouds, mClouds2, mClouds3, mBackgrnd;
	Model mBuildings[11][11];

private:
struct threadData
	{
		std::future<void> loader;
		int totalToLoad = 0;
		int loadedSoFar = 0;
		bool running = false;
	};
	threadData mThreadData;
	DirectX::SpriteFont * pFont = nullptr;
	DirectX::SpriteBatch *pFontBatch = nullptr;
	void load();
	void renderLoad(float dTime);

	float gAngle = 0;
};

#endif




