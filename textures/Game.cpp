#include "WindowUtils.h"
#include "D3D.h"
#include "Game.h"
#include "GeometryBuilder.h"

using namespace std;
using namespace DirectX;
using namespace DirectX::SimpleMath;

void Game::load()
{
	MyD3D& d3d = WinUtil::Get().GetD3D();
}

void Game::Initialise()
{
	MyD3D& d3d = WinUtil::Get().GetD3D();
	pFontBatch = new SpriteBatch(&d3d.GetDeviceCtx());
	assert(pFontBatch);
	pFont = new SpriteFont(&d3d.GetDevice(), L"../bin/data/algerian.spritefont");
	assert(pFont);

	mThreadData.totalToLoad = 2;
	mThreadData.loadedSoFar = 0;
	mThreadData.running = true;
	mThreadData.loader = std::async(launch::async, &Game::load, this);
	float gAngle = 0.f;

	mQuad.Initialise(BuildQuad(d3d.GetMeshMgr()));
	mBox.Initialise(BuildCube(d3d.GetMeshMgr()));
	mClouds = mQuad;
	mClouds2 = mClouds;
	mClouds3 = mClouds;
	mBackgrnd = mQuad;
	string buildingTex[4] = { "building1.dds", "building2.dds", "building3.dds", "building4.dds" };

	Material mat;

	float posX = 1.2f, posY, posZ = 1.8;
	int randTex;
	for (int i = 1; i < 11; i++)
	{
		for (int j = 1; j < 11; j++)
		{
			mat = mQuad.GetMesh().GetSubMesh(0).material;
			randTex = rand() % 4;
			posY = (rand() % 6);
			if (posY < 2)	posY = 3;
			posY += ((rand() % 5));
			posY = posY / 5.5f;
			mBuildings[i][j] = mBox;
			mBuildings[i][j].GetScale() = Vector3(0.3f, posY, 0.1f);
			mBuildings[i][j].GetPosition() = Vector3(posX, -0.4f, posZ);
			posX -= 0.8f;
			mat.pTextureRV = d3d.GetCache().LoadTexture(&d3d.GetDevice(), buildingTex[randTex]);
			mat.texture = buildingTex[randTex];
			mBuildings[i][j].SetOverrideMat(&mat);
		}
		posX = 1.2f;
		posZ -= 0.5f;
	}

	mat.pTextureRV = d3d.GetCache().LoadTexture(&d3d.GetDevice(), "cloud.dds");
	mat.texture = "cloud";
	mat.flags |= Material::TFlags::ALPHA_TRANSPARENCY;
	mClouds.SetOverrideMat(&mat);

	mClouds2.SetOverrideMat(&mat);

	mClouds3.SetOverrideMat(&mat);

	//wood floor
	mQuad.GetScale() = Vector3(5, 1, 5);
	mQuad.GetPosition() = Vector3(0, -1, 0);
	mat = mQuad.GetMesh().GetSubMesh(0).material;
	mat.gfxData.Set(Vector4(0.9f, 0.8f, 0.8f, 0), Vector4(0.9f, 0.8f, 0.8f, 0), Vector4(0.9f, 0.8f, 0.8f, 1));
	mat.pTextureRV = d3d.GetCache().LoadTexture(&d3d.GetDevice(), "ground.dds");
	mat.texture = "ground.dds";
	mQuad.SetOverrideMat(&mat);

	//Background
	mBackgrnd.GetScale() = Vector3(8, 7, 5);
	mBackgrnd.GetPosition() = Vector3(1.5, 1, 3);
	mBackgrnd.GetRotation() = Vector3(-1.5708f, 0, 0);
	mat.pTextureRV = d3d.GetCache().LoadTexture(&d3d.GetDevice(), "mountain_backdrop.dds");
	mat.texture = "mountain_backdrop";
	mBackgrnd.SetOverrideMat(&mat);

	d3d.GetFX().SetupDirectionalLight(0, true, Vector3(-0.7f, -0.7f, 0.7f), Vector3(0.47f, 0.47f, 0.47f), Vector3(0.15f, 0.15f, 0.15f), Vector3(0.25f, 0.25f, 0.25f));

}

void Game::Release()
{
	delete pFontBatch;
	pFontBatch = nullptr;
	delete pFont;
	pFont = nullptr;
}

void Game::Update(float dTime)
{
	gAngle += dTime * 0.1f;
}

void Game::Render(float dTime)
{
	if (mThreadData.running)
	{
		if (!mThreadData.loader._Is_ready())
		{
			renderLoad(dTime);
			return;
		}
		mThreadData.loader.get();
		mThreadData.running = false;
	}


	MyD3D& d3d = WinUtil::Get().GetD3D();
	d3d.BeginRender(Colours::Black);
	float alpha = 0.5f + sinf(gAngle * 2)*0.5f;


	d3d.GetFX().SetPerFrameConsts(d3d.GetDeviceCtx(), mCamPos);

	CreateViewMatrix(d3d.GetFX().GetViewMatrix(), mCamPos, Vector3(0, 0, 0), Vector3(0, 1, 0));
	CreateProjectionMatrix(d3d.GetFX().GetProjectionMatrix(), 0.25f*PI, WinUtil::Get().GetAspectRatio(), 1, 1000.f);
	Matrix w = Matrix::CreateRotationY(sinf(gAngle));
	d3d.GetFX().SetPerObjConsts(d3d.GetDeviceCtx(), w);

	//floor
	mQuad.GetRotation() = Vector3(0, 0, 0);
	mQuad.GetScale() = Vector3(8, 2, 4);
	mQuad.GetPosition() = Vector3(-1, -1, -1.4f);
	d3d.GetFX().Render(mQuad);

	for (int i = 1; i < 11; i++)
	{
		for (int j = 1; j < 11; j++)
		{
			d3d.GetFX().Render(mBuildings[i][j]);
		}
	}

	//Rendering the background
	d3d.GetFX().Render(mBackgrnd);

	//main cube - forced transparency under pogram control
	Vector3 dir = Vector3(1, 0, 0);
	Matrix m = Matrix::CreateRotationY(gAngle);
	dir = dir.TransformNormal(dir, m);
	dir *= -1;

	mClouds.GetRotation() = Vector3(-1.5708f, 0, 0);
	mClouds.GetPosition() = Vector3(0 + alpha, 2, 0);
	mClouds.HasOverrideMat()->flags &= ~Material::TFlags::CCW_WINDING;
	d3d.GetFX().Render(mClouds);
	mClouds.HasOverrideMat()->flags |= Material::TFlags::CCW_WINDING;
	d3d.GetFX().Render(mClouds);

	mClouds2.GetRotation() = Vector3(-1.5708f, 0, 0);
	mClouds2.GetPosition() = Vector3(-4 + alpha, 2.5, 0);
	mClouds2.HasOverrideMat()->flags &= ~Material::TFlags::CCW_WINDING;
	d3d.GetFX().Render(mClouds2);
	mClouds2.HasOverrideMat()->flags |= Material::TFlags::CCW_WINDING;
	d3d.GetFX().Render(mClouds2);

	mClouds3.GetRotation() = Vector3(-1.5708f, 0, 0);
	mClouds3.GetPosition() = Vector3(-2.5 + alpha, 1.5, -0.5);
	mClouds3.HasOverrideMat()->flags &= ~Material::TFlags::CCW_WINDING;
	d3d.GetFX().Render(mClouds3);

	mClouds3.HasOverrideMat()->flags |= Material::TFlags::CCW_WINDING;
	d3d.GetFX().Render(mClouds3);



	d3d.EndRender();
}

void Game::renderLoad(float dTime)
{
	MyD3D& d3d = WinUtil::Get().GetD3D();
	d3d.BeginRender(Colours::Black);
	pFontBatch->Begin();
	static int pips = 0;
	static float elapsed = 0;
	elapsed += dTime;
	if (elapsed > 0.25f)
	{
		pips++;
		elapsed = 0;
	}

	if (pips > 10)
	{
		pips = 0;
	}

	wstringstream ss;
	ss << L"Loading meshes(" << (int)(((float)mThreadData.loadedSoFar / (float)mThreadData.totalToLoad) * 100.f) << L"%) ";
	for (int i = 0; i < pips; i++)
	{
		ss << L".";
	}
	pFont->DrawString(pFontBatch, ss.str().c_str(), Vector2(10, 550), Colours::White, 0, Vector2(0, 0), Vector2(0.5f, 0.5f));
	pFontBatch->End();
	d3d.EndRender();
}

LRESULT Game::WindowsMssgHandler(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	const float camInc = 200.f * GetElapsedSec();
	//do something game specific here
	switch (msg)
	{
		// Respond to a keyboard event.
	case WM_CHAR:
		switch (wParam)
		{
		case 27:
		case 'q':
		case 'Q':
			PostQuitMessage(0);
			return 0;
		case 'a':
			mCamPos.y += camInc;
			break;
		case 'z':
			mCamPos.y -= camInc;
			break;
		case 'd':
			mCamPos.x -= camInc;
			break;
		case 'f':
			mCamPos.x += camInc;
			break;
		case 'w':
			mCamPos.z += camInc;
			break;
		case 's':
			mCamPos.z -= camInc;
			break;
		case ' ':
			mCamPos = mDefCamPos;
			break;
		}
	}
	//default message handling (resize window, full screen, etc)
	return WinUtil::Get().DefaultMssgHandler(hwnd, msg, wParam, lParam);
}



